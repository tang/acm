from __future__ import print_function
#!/usr/bin/env python

# @file     acmSetup.py
# @purpose  main CLI for general-purpose athena scripts (for use with git+cmake)
# @author   Vincent Pascuzzi <vincent.pascuzzi@cern.ch, vpascuzz@physics.utoronto.ca>
# @date     March 2017

import os
import argparse
import string
import subprocess
import sys
from utils import (acm_print, acmsetup_search, ask_question, get_exports, get_unsets, mkdir, write_setup_file, write_unsetup_file)
from distutils import spawn

# acm framework exports
ACMDIR      = os.environ['ACMDIR'] #picked up from the acmSetup.sh script
ACMBIN      = "%s/bin" % ACMDIR
ACMPYTHON   = "%s/python" % ACMDIR
ACMSCRIPTS  = "%s/scripts" % ACMDIR

parser = argparse.ArgumentParser(description='Main CLI for general-purpose athena scripts')
parser.add_argument('--sourcedir', action='store', metavar='sourcedir', required=False, default="", dest='sourcedir', help='See sourcearea option (sourcedir is here for historical reasons)')
parser.add_argument('--sourcearea', action='store', metavar='sourcedir', required=False, default="", dest='sourcedir', help='the source directory, i.e. where the source code (packages) are located (default: ../source)')
parser.add_argument('--sourcerepo', action='store', metavar='sourcerepo', required=False, default="", dest='sourcerepo', help='optional: the git repository you want to clone. If no sourcedir specified, will clone into the directory above')
parser.add_argument('--sourcebranch',action='store',metavar='sourcebranch',required=False,default="",dest='sourcebranch',help='optional: use with sourcerepo option to specify a specific branch to clone for your SourceArea')
parser.add_argument('--testarea', action='store', metavar='builddir', required=False, default="", dest='builddir', help='the build directory, i.e. where the compilation objects will be stored (default: ./)')
parser.add_argument('--search', action='store_true', required=False, dest='do_search', help='search parent directories of the current working directory for a previously set acm workarea')
parser.add_argument('--with-ssh', action='store_true', required=False, dest='with_ssh', help='use ssh for gitlab instead of https/kerberos (default)')
parser.add_argument('--local-setup', action='store_true', required=False, dest='local_setup', help='force use of locally installed releases')
parser.add_argument('--releasearea', action='store', metavar='releasinstalledir', required=False,
                    default=None,
                    dest='release_install_dir', help='the directory containing an ATLAS analysis release installation; '
                                               'typically used only when using acmSetup on a local machine, i.e. not lxplus or other ATLAS(asetup)-equipped node')
parser.add_argument('-o', '--outfile', default=None, dest='configFilename', help=argparse.SUPPRESS)
parser.add_argument('--setup', action='store', metavar='setup', required=False, default=False, dest='setup', help='setup ACM in the current working directory. If an existing .acmsetup.save is present, its configuration is loaded; else, a new workarea is set up')
parser.add_argument('--unset', action='store_const', const=True, required=False, dest='unSetup', help='unset the current ACM environment')
parser.add_argument('--docker',action='store_true',required=False,dest='doDocker',help='EXPERIMENTAL docker support')
parser.add_argument('-p', '--path', action='store', metavar='myacmpath', required=False, nargs=1, default=os.getcwd(), dest='myacmpath', help='the path where ACM should be setup. Default is the current working directory')
parser.add_argument('asetupArguments', nargs="*", default=[], help="Arguments passed to asetup")

## Sets up a workarea for the first time.
def setup_workarea_dirs():
  sourcePath ="%s/../source" % args.myacmpath # changed default to a source dir above the build dir
  buildPath = args.myacmpath # changed default to the current directory
  userdefined_sourcedir = False
  userdefined_builddir = False


  #if there is an .acmsetup.save then load the values
  acmSave = {}
  changedAsetupArguments=False
  if os.path.isfile(args.myacmpath+"/.acmsetup.save") :
    with open(args.myacmpath+"/.acmsetup.save") as f:
      for line in f:
        (key, val) = line.split("=")
        acmSave[key.strip()] = val.strip()
  if len(args.asetupArguments)==0 and "asetupArguments" in acmSave:
    args.asetupArguments += [acmSave["asetupArguments"]]
  elif "asetupArguments" in acmSave and acmSave["asetupArguments"] != (" ".join(args.asetupArguments)):
    changedAsetupArguments=True

  # check if the sourcedir and builddir arguments have been set by comparing with
  # the default values
  if args.sourcedir != "" :
    sourcePath = os.path.abspath(args.sourcedir)
    userdefined_sourcedir = True
  else :
    #try to load from the .acmsetup.save
    if "SourceArea" in acmSave:
      sourcePath = acmSave['SourceArea']
      args.sourcedir = sourcePath
      userdefined_sourcedir = True
    elif args.sourcerepo=="" and not os.path.isdir(sourcePath):
      # run in sourceless mode only if sourcerepo not specified and default sourcePath (../source) not existing
      return (None, None, None)


  if args.builddir != "" :
    buildPath = os.path.abspath(args.builddir)
    userdefined_builddir = True
  else:
    if "TestArea" in acmSave:
      buildPath = acmSave['TestArea']
    else:
      #if nothing specified, use the PWD
      #acm_print("No --builddir set...using %s!" % os.getcwd())
      buildPath = os.getcwd()

  # check if source and build dirs existing
  # get a definitive answer from the user

  if args.sourcerepo!="":
    if not userdefined_sourcedir:
      #extract the sourcedir from the sourcerepo name 
      if args.sourcerepo.endswith(".git"):
        sourcePath = args.sourcerepo[:-4]
      else:
        sourcePath = args.sourcerepo
      sourcePath = sourcePath.rsplit('/',1)[-1]
      sourcePath = "%s/../%s" % (args.myacmpath,sourcePath)
      userdefined_sourcedir = True
    
    if os.path.isdir(sourcePath):
      acm_print("WARNING: Cannot clone repo because %s already exists" % sourcePath)
    else:
      if not os.path.isdir(args.sourcerepo):
       #guess that it is a gitlab path ... 
       if "gitlab.cern.ch" not in args.sourcerepo and "http://" not in args.sourcerepo and "https://" not in args.sourcerepo and "ssh://" not in args.sourcerepo: 
         prefixPart = os.environ.get("ACM_GITLAB_PATH_PREFIX","https://:@gitlab.cern.ch:8443/")
         if args.with_ssh: prefixPart = "ssh://git@gitlab.cern.ch:7999/"
         args.sourcerepo = prefixPart + args.sourcerepo
       if not args.sourcerepo.endswith(".git"): args.sourcerepo = args.sourcerepo + ".git"

       #FIXME : This is horrible bit seems like we need a recent git version to do recursive checkouts properly with a branch specified!
      if args.sourcebranch!="":
        myCmd = "$ATLAS_LOCAL_ROOT_BASE/x86_64/git/2.11.1-x86_64-slc6/bin/git clone --recursive -b %s %s %s" % (args.sourcebranch,args.sourcerepo,sourcePath)
      else:
        myCmd = "$ATLAS_LOCAL_ROOT_BASE/x86_64/git/2.11.1-x86_64-slc6/bin/git clone --recursive %s %s" % (args.sourcerepo,sourcePath)
      acm_print("Executing: %s" % myCmd)
      import subprocess
      ret = subprocess.call(myCmd,shell=True)
      if ret!=0:
        acm_print("ERROR cloning respository: %s ... see messages from git above" % args.sourcerepo)
        exit(ret)
    

  acm_print("SourceArea = %s" % (sourcePath))
  acm_print("TestArea = %s" % (buildPath))

  if not os.path.isdir(sourcePath) and userdefined_sourcedir:
    question = "ACM\tSpecified --sourcedir '%s' does not exist in %s. Create it?" % (args.sourcedir, args.myacmpath)
    answer = ask_question(question,["y","n"])
    if answer == "y" :
      acm_print ("Creating source directory '%s' in '%s'" % (args.sourcedir, args.myacmpath))
      mkdir(sourcePath)
    else :
      question = "ACM\tUse default source directory, 'source', then?"
      answer = ask_question(question, ["y","n"])
      if answer == "y" :
        sourcePath ="%s/source" % args.myacmpath
        if not os.path.isdir(sourcePath) :
          acm_print ("Creating source directory '%s' in '%s'" % (sourcePath, args.myacmpath))
          mkdir(sourcePath)
      else :
        acm_print ("Figure out what you want and try again later.")
        exit(0)
  elif not os.path.isdir(sourcePath) :
    acm_print ("Creating source directory '%s'" % (sourcePath))
    mkdir(sourcePath)
  if not os.path.isdir(buildPath) and userdefined_builddir:
    question = "Specified builddir '%s' does not exist in %s. Create it?" % (args.builddir, args.myacmpath)
    answer = ask_question(question,["y","n"])
    if answer == "y" :
      acm_print ("Creating build directory '%s' in '%s'" % (buildPath, args.myacmpath))
      mkdir(buildPath)
    else :
      question = "Use default build directory, 'build', then?"
      answer = ask_question(question, ["y","n"])
      if answer == "y" :
        sourcePath ="%s/build" % args.myacmpath
        mkdir(buildPath)
      else :
        acm_print ("Figure out what you want and try again later.")
        exit(0)
  elif not os.path.isdir(buildPath) :
    acm_print ("Creating build directory '%s' in '%s'" % (args.builddir, args.myacmpath))
    mkdir(buildPath)

  acm_print ("Workarea directories set.")
  return (sourcePath, buildPath,changedAsetupArguments)

## Writes the ACM configuration file to `path`
def write_acmsetupsave(path,sourcePath,buildPath, gitlabprefix) :
  f = None
  fname = ".acmsetup.save"
  # Check if the config file already exists.
  #if not os.path.isfile(path+"/"+fname) :
  myValues = {"SourceArea":sourcePath,"TestArea":buildPath,"ACM_GITLAB_PATH_PREFIX":gitlabprefix,"asetupArguments":" ".join(args.asetupArguments)}
  try :
    needUpdate=True
    if os.path.isfile(".acmsetup.save") :
      needUpdate=False
      acmSave=dict()
      with open(".acmsetup.save") as f:
        for line in f:
          (key, val) = line.split("=")
          acmSave[key.strip()] = val.strip()
      #check if we need to update
      if myValues!=acmSave: needUpdate=True
    if needUpdate:
      f = open ('.acmsetup.save', 'w')
      f.write("SourceArea = %s\nTestArea = %s\n" % (sourcePath,buildPath))
      f.write("ACM_GITLAB_PATH_PREFIX = %s\nasetupArguments = %s\n" % (gitlabprefix," ".join(args.asetupArguments)))
      f.close()
  except OSError :
    acm_print ("Unable to open .acmsetup.save to %s!" % path)
    sys.exit(1)

## TODO: implement
#def update_acmsetupsave(path) :

## TODO: implement
#def read_acmsetupsave(path) :


## Sets up a new acm workarea
def setup_acm_workarea(path, updateAcmSave=True):
  # Check if `setupATLAS`
  try:
    hostname = os.uname()[1] # portable way to get the machine's hostname
  except:
    acm_print("Could not determine hostname (please report error to hn-atlas-PATHelp@cern.ch)!")
    sys.exit(0)
  asetup_check = ""
  if not args.doDocker and not is_asetup():
    args.local_setup = True #force local setup
    if args.release_install_dir is None and "ReleaseArea" not in os.environ and "ReleaseSetupScript" not in os.environ:
      acm_print("Could not find the asetup command and a release area is not specified. Please do one of the following:\n"
                "\t- if you are on lxplus, do: `setupATLAS` and try again.\n"
                "\t- if you have cvmfs access, do: source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh\n"
                "\t- if you have installed releases locally, set the `ReleaseArea` env var to point to the location of these releases\n"
                "\t\tor you can use the --releasearea option of acmSetup to specify the location")
      sys.exit(0)

  sourcePath, buildPath, changedAsetupArguments = setup_workarea_dirs()
  if sourcePath is None and buildPath is None :
    # We are running in "sourceless" mode. Only set up release.
    write_setup_file(None, args.configFilename," ".join(args.asetupArguments), None)
    return

  ### set the gitlab path prefix (default is https)
  gitlabprefix="https://:@gitlab.cern.ch:8443/"
  ### check args if ssh support is desired
  if args.with_ssh :
    gitlabprefix="ssh://git@gitlab.cern.ch:7999/"
  ### check .acmsetup.save, if it exists, for gitlabprefix
  elif os.path.isfile(args.myacmpath+"/.acmsetup.save") :
    if 'ssh://git@gitlab.cern.ch' in open(args.myacmpath+"/.acmsetup.save") :
      gitlabprefix="ssh://git@gitlab.cern.ch:7999/"
  setupText = get_exports(sourcePath, buildPath, ACMBIN, ACMPYTHON, ACMSCRIPTS, gitlabprefix)  # type: string

  doLocal=""
  doDocker=""

  if args.local_setup or args.doDocker:
    release_type, release = parseAsetupArgs()
    if "ReleaseArea" not in os.environ and args.release_install_dir != None: 
      doLocal += "export ReleaseArea="+args.release_install_dir+"\n"
    doLocal += """echo "ACM\t export AtlasProject=%s"\n""" % release_type
    doLocal += """echo "ACM\t export AtlasVersion=%s"\n""" % release
    doLocal += "export AtlasProject=%s\n" % release_type
    doLocal += "export AtlasVersion=%s\n" % release
    if args.doDocker:
      doDocker = "atlas/%s:%s" % (release_type.lower(),release)
      

  else:
    setupText += asetup_check+'\n'
  
  if updateAcmSave:
    write_acmsetupsave(path,sourcePath,buildPath, gitlabprefix)
  write_setup_file(setupText, args.configFilename," ".join(args.asetupArguments),sourcePath,changedAsetupArguments,doLocal,doDocker)
  pass


def parseAsetupArgs():
  release_type = None
  release = None
  for e in args.asetupArguments[0].split(','):
    if e[0].isdigit():
      #assume this is the release number
      if release is None:
        release = e
        pass
      else:
        acm_print("ERROR: malformed option list! Can only have one release number. Found %s and %s" % (release,e))
        sys.exit(0)
        pass
    else:
      if release_type is None:
        release_type = e
        pass
      else:
        acm_print("ERROR: malformed option list! Should only contain one release type. Found %s and %s" % (release_type,e))
        sys.exit(0)
        pass
    pass
  return release_type, release


def is_asetup() :
  try :
    if args.local_setup: return False #user has forced local setup on us
    ass = os.environ["AtlasSetupSite"]
    if "asetup" in ass:
      return True
    else :
      return False
  except:
    return False
  pass

# We already have an acm environment setup, but want to setup a new one
def reset(acmWorkareaPath) :
  acm_print ("Unsetting current ACM environment.")
  unsets = get_unsets(ACMBIN, ACMPYTHON, ACMSCRIPTS)
  write_unsetup_file(unsets, args.configFilename)
  # Setup acm
  acm_print ("Setting up ACM workarea in %s" % acmWorkareaPath)
  setup_acm_workarea(acmWorkareaPath)

## C(++)-style main
def main() :
  # Get the current acm environment path (could be None)
  myacm = os.getenv("ACMBIN")
  acmWorkareaPath = os.getcwd()


  # Here we check if any arguments were provided.
  # First, check if there's already an existing ACM environment set up. If not,
  # the default is to search the cwd for a .acmsetup.save (i.e. the cwd has been
  # previously acmsetup); no .acmsetup.save, a new workarea is created. If an acm
  # environment is already set up, print a message and exit.
  
  if not len(sys.argv) > 3 or args.setup or args.asetupArguments==[] :
    # No args provided or a new acmsetup was requested ==> new setup; ensure existing
    # environment is not yet setup.
    if myacm is None or myacm == "" :
      if os.path.isfile(acmWorkareaPath+"/.acmsetup.save") :
        # Load existing setup from .acmsetup.save
        acm_print ("Loading existing ACM workarea found in current working directory.")
        setup_acm_workarea(acmWorkareaPath,False)
        sys.exit(0)
      else:
        # No existing workarea setup in myacmpath.
        # Check if `asetupArguments` is set. If not, we exit gracefully.
        if (args.asetupArguments == []) :
          acm_print ("No .acmsetup found and no asetup arguments provided--cannot continue to setup acm!")
          acm_print ("You should pass a flavour of analysis and release number as arguments, for example:\n$ acmsetup AthAnalysis,21.2.3")
          sys.exit(0)
        acm_print ("Setting up ACM workarea in %s" % acmWorkareaPath)
        setup_acm_workarea(acmWorkareaPath)
        sys.exit(0)
    else :
      # We already have an acm environment setup, but want to setup a new one
      reset(acmWorkareaPath)
  else :
    # Does the user want to search for a previously setup acm?
    if args.do_search:
      # The search flag is set ==> Search for an existing .acmsetup.save in the cwd and
      # all parents up to the root directory.
      searchPath = acmsetup_search()
      # .acmsetup.save was found; ask the user to use it or not.
      if searchPath is not None :
        acm_print ("Found existing ACM setup in %s" % searchPath)
        question = "Use the existing setup?"
        # Spam user for only an acceptable answer.
        answer = ask_question(question, ["y","n"])
        # Use found setup.
        if answer == "y" :
          acmWorkareaPath = searchPath
          acm_print ("Setting up ACM environment in %s" % acmWorkareaPath)
          load_acm_workarea(acmWorkAreaPath)
          sys.exit(0)
        # Do not use the found setup -- just exit.
        else :
          acm_print ("acmsetup exiting gracefully.")
          sys.exit(0)
    # Nothing special to do; setup acm using arguments provided
    else :
      if myacm is None or myacm == "" :
        # Check if user wants to run locally, i.e. not on an asetup-aware node
        if args.local_setup:
          if args.release_install_dir is None and "ReleaseArea" not in os.environ and "ReleaseSetupScript" not in os.environ:
            acm_print("It appears you want to perform a local setup, however you have not specified "
                      "--release_install_dir or set the ReleaseArea or ReleaseSetupScript environment variables "
                      "<path>.\n\tPlease correct this and try again.")
            sys.exit(0)
            return
          else: # we have a release dir or release setup script ...
            if "ReleaseSetupScript" in os.environ:
              acm_print("Setting up ACM workarea in %s" % acmWorkareaPath)
              setup_acm_workarea(acmWorkareaPath, True)

            else:
              # Little sanity-check to ensure the release dir specified is OK
              if args.release_install_dir is None:
                args.release_install_dir = os.environ["ReleaseArea"]
              try:
                  acm_print("Release metadata: ")
                  with open(args.release_install_dir+'/ReleaseData', 'r') as release_data:
                    print(release_data.read())
                    pass
              except:
                print("none")
                acm_print("WARNING: Could not find release metadata! Release may not be installed properly (or you\n"
                        "\tremoved 'ReleaseData' from the release install directory).")
              acm_print("Setting up ACM workarea in %s" % acmWorkareaPath)
              setup_acm_workarea(acmWorkareaPath, True)
        else :
          # Setup acm with provided arguments
          acm_print ("Setting up ACM workarea in %s" % acmWorkareaPath)
          setup_acm_workarea(acmWorkareaPath)
          sys.exit(0)
      else:
        # We already have an acm environment setup, but want to setup a new one
        reset(acmWorkareaPath)
        sys.exit(0)


## Main entry point to script.
if __name__ == "__main__" :
  args = parser.parse_args()
  sys.exit(main())
