# @file     acmcommands.py
# @purpose  Provides commands list, help text, and command functions to acm.
# @author   Vincent Pascuzzi <vincent.pascuzzi@cern.ch, vpascuzz@physics.utoronto.ca>
# @date     March 2017

import sys
import glob


###
#TODO: move all `acm_print`s outside this script
###

## Standard commands dictionary for acm module.
## The dictionary keys are supplied to acm as available commands, and the
## values are the corresponding functions that are called when the command
## is provided as an acm argument.
## TODO: add more standard commands
def get_std_cmds_dict() :
  std_cmds_dict = {
      "clean"                 : acm_clean,
      "find_packages"         : acm_find_packages,
      "compile"               : acm_compile,
      "compile_pkg"           : acm_compile_pkg,
      "new_pkg"               : acm_new_pkg,
      "new_skeleton"          : acm_new_skeleton,
      "clone_project"         : acm_clone_project,
      "sparse_clone_project"  : acm_sparse_clone_project,
      "add_pkg"               : acm_add_pkg,
      "include_pkg"           : acm_add_pkg,
      "add_pkg_clients"       : acm_add_pkg_clients,
      "rm_pkg"                : acm_rm_pkg,
      "exclude_pkg"           : acm_rm_pkg,
      "switch"                : acm_switch,
      "test"                  : acm_test
      }
  return std_cmds_dict

## Standard commands help dictionary for acm.
## The dictionary keys and values are supplied to acm to create the
## -h/--help menu.
def get_std_cmds_help() :
  std_cmds_help = {
      "clean"           : "\n\t\tclean entire workspace (use -f option to reconfigure too)",
      "find_packages"   : "[<package_filter_file>]\n\t\treconfigure the workspace, accounting for package filters",
      "compile"         : "\n\t\tcompile entire workspace",
      "compile_pkg"     : "<pkg>\n\t\tcompile package <pkg>",
      "new_pkg"         : "<pkg>\n\t\tcreate a new package <pkg>",
      "new_skeleton"    : "<pkg>\n\t\tcreates a new package <pkg> with an algorithm <pkg>Alg and a joboption <pkg>AlgJobOptions.py",
      "clone_project"   : "<name> <repo> [<branch/tag>]\n\t\tclones an existing git repo <repo> into directory $SourceArea/<name>. If $SourceArea is a repo, then <repo> is added as a submodule. Optionally checks out at specific branch/tag. Special treatment in case of <name>=athena.",
      "sparse_clone_project" : "\n\t\tSpecial method for sparse clone for the athena repo",
      "add_pkg"         : "<pkg>\n\t\tupdates the filter file so that <pkg> is included in compilation (synonym for include_pkg)" ,
      "include_pkg"     : "<pkg>\n\t\tupdates the filter file so that <pkg> is included in compilation (synonym for add_pkg)" ,
      "add_pkg_clients" : "<pkg>\n\t\tcalls add_pkg for all clients of <pkg>. Use this to recompile all clients of a package",
      "rm_pkg"          : "<pkg>\n\t\tadds <pkg> to the filter file so that <pkg> is excluded from compilation (synonym for exclude_pkg)",
      "exclude_pkg"     : "<pkg>\n\t\tadds <pkg> to the filter file so that <pkg> is excluded from compilation (synonym for exclude_pkg)",
      "switch"          : "<hash/branch/tag> <path>\n\t\tswitch <path> (must be in a git repo) to given <hash/branch/tag>",
      "test"            : "<pkg>\n\t\truns the ctest for this package"
      }
  return std_cmds_help

def acm_test(argv):
  from utils import acm_print
  import subprocess
  
  if len(argv)>0:  cmd = "cd $TestArea; ctest --output-on-failure --label-regex %s" % argv[0]
  else: cmd = "cd $TestArea; ctest --output-on-failure"
  acm_print("Executing: %s ... " % cmd)
  ret = subprocess.call(cmd,shell=True)

  return ret



def acm_switch(argv):
  if len(argv)==1 and argv[0]=="-h":
    print("switch <branch/tag> <package>: ")
    print("""  Change a package content to match tag as present in <branch>.
  The package must be inside a repository (or project).

  This is achieved with a git checkout followed by a git clean.

  WARNING: Your switches may get reversed by some calls to add_pkg.

 Examples:

   - acm switch upstream/master athena/Control/AthAnalysisBaseComps : moves package in athena/Control/AthAnalysisBaseComps to match content of the upstream master

   - acm switch tags/release/21.2.0 athena/Control/AthAnalysisBaseComps : moves package to match content in release 21.2.0

""")
    return 0


  #this command assumes you are going to switch part of a git repo
  #it performs a git checkout and a git clean
  from utils import acm_print
  import os
  import subprocess


  #cmd = "cd $SourceArea/%s; cd ../;git rm -rf ~-/. && cd $SourceArea && mkdir -p $SourceArea/%s" % (argv[1],argv[1])


  #had to remake the directory because it gets deleted by git when its emptied by above rm
  cmd = "cd $SourceArea/%s; git checkout %s -- ." % (argv[1],argv[0])

  cmd2 = "cd $SourceArea/%s; git clean -f -- ." % (argv[1])

  acm_print("Executing: %s ... " % cmd)
  ret = subprocess.call(cmd,shell=True)
  acm_print("Executing: %s ... " % cmd2)
  ret2 = subprocess.call(cmd2,shell=True)

  sys.exit(0)




def clone_project(argv,doSparse=False):
  projName = argv[0]

  projTag = None

  from utils import acm_print
  import os

  gitlabPrefix = os.environ.get("ACM_GITLAB_PATH_PREFIX","https://:@gitlab.cern.ch:8443/")
  if not gitlabPrefix.endswith("/"): gitlabPrefix += "/";

  if len(argv)<2:
    #check for known projects
    if projName=="athena": 
      projPath = gitlabPrefix + "$USER/athena.git"
      #if user has specified gitlabusername in git config then use that instead
    else:
      #assume the first argument is actually the projPath
      #extract the projName from the path ...
      #strip any .git from the end and then take the last part as the projName
      projPath = str(projName)
      projName = projName.rsplit('/',1)[-1]
      if projName.endswith(".git"): projName = projName[:-4]
      
  else :
    projPath = argv[1]

  if len(argv)>2: projTag=argv[2]


  #if path doesn't contain ://, then append standard path
  if "://" not in projPath:
    #first check if it's a local repo ... i.e. check existence of .git in projPath
    if not os.path.exists(projPath+"/.git"):
      projPath = gitlabPrefix + projPath
      #FIXME: what about ssh://:@gitlab.cern.ch:7999/ ??

  

  fullProjName = str(projName)
  basePath = os.environ['SourceArea']
  if '/' in projName:
    #extend basePath with root of projName
    basePath = basePath + "/" + projName.rsplit('/',1)[0]
    projName = projName.rsplit('/',1)[1]

  #check project doesn't already exist
  if os.path.exists(basePath+"/"+projName):
    acm_print("ERROR: File or Directory called %s already exists in SourceArea" % projName)
    sys.exit(-1)

  #check if basePath is a repo (has a .git dir)
  isRepo = os.path.isdir(basePath+"/.git")

  import subprocess
  
  if doSparse:
    #special case ... do sparse setup of workdir
    cmd = "cd $SourceArea; git atlas init-workdir %s %s" % (projPath,projName)
    acm_print("Executing: %s ... " % cmd)
    ret = subprocess.call(cmd,shell=True)
    if ret!=0:
      acm_print("ERROR: Failed to sparse checkout %s project ... please check you specified a valid repository path (tried %s). Check above for error messages!" % (projName,projPath))
      #delete the directory to clean up after failure
      import shutil
      shutil.rmtree(basePath+"/"+projName)
      sys.exit(ret)

    #switch to branch/tag if given
    if projTag != None:
      cmd = "cd $SourceArea/%s; git checkout -B %s origin/%s" % (projName,projTag,projTag)
      acm_print("Executing: %s ... " % cmd)
      ret = subprocess.call(cmd,shell=True)

  else:

    if not isRepo:
      #just clone the repo
      if projTag != None:
        cmd = "git clone --recursive --branch %s %s %s/%s " % (projTag,projPath,basePath,projName)
      else:
        cmd = "git clone --recursive %s %s/%s " % (projPath,basePath,projName)
      acm_print("Executing: %s ... " % cmd)
      ret = subprocess.call(cmd,shell=True)
    else:
      #add as submodule
      cmd = "cd %s;git submodule add %s %s ; git submodule update --init --recursive " % (basePath,projPath,projName)
      acm_print("Executing: %s ... " % cmd)
      ret = subprocess.call(cmd,shell=True)
      if ret==0 and projTag != None:
        #need to switch into given tag/branch
        cmd = "cd %s/%s; git checkout %s" % (basePath,projName,projTag) #FIXME: should we git commit this change to the user's repo?
        ret = subprocess.call(cmd,shell=True)

    if ret!=0:
      acm_print("ERROR: unable to set up project .. see errors from git, above")
      sys.exit(ret)

  if projName=="athena" and not doSparse:
    #special case ... add the upstream and adjust the origin to the gitlab remote
    acm_print("Adding upstream remote for athena ... ")
    subprocess.call("cd %s/athena;git remote add upstream %satlas/athena.git;git fetch upstream" % (basePath,gitlabPrefix),shell=True)
    #adjust origin, if required (look for gitlab.cern.ch in path name)
    if "gitlab.cern.ch" not in projPath:
      acm_print("Adjusting origin remote to gitlab fork ... ")
      subprocess.call("cd %s/athena;git remote rename origin cache;git remote add origin %s$USER/athena.git" % (basePath,gitlabPrefix),shell=True)


  if projName=="athena":
    ret = 0
    if projTag != None:
      acm_print("Switching athena to branch/tag %s ..." % projTag)
      mycmd = "cd %s/athena;git fetch upstream;git checkout %s" % (basePath,projTag)
      acm_print("Executing: %s" % mycmd)
      ret = subprocess.call(mycmd,shell=True)
    #special case ... synchronize the version with the setup release
    elif "AtlasBuildStamp" in os.environ and "AtlasBuildBranch" in os.environ and os.environ.get("AtlasReleaseType","")=="nightly":
      #is a nightly .. switch to AtlasBuildBranch
      acm_print("Switching athena project to branch (updating upstream too) %s ..." % os.environ['AtlasBuildBranch'])
      mycmd = "cd %s/athena;git fetch upstream;git checkout -B $AtlasBuildBranch upstream/$AtlasBuildBranch" % basePath
      acm_print("Executing: %s" % mycmd)
      ret = subprocess.call(mycmd,shell=True);
    elif "AtlasVersion" in os.environ:
      acm_print("Switching athena project to tag %s" % os.environ['AtlasVersion'])
      mycmd = "cd %s/athena;git checkout -B $AtlasVersion release/$AtlasVersion" % basePath
      acm_print("Executing: %s" % mycmd)
      ret = subprocess.call(mycmd,shell=True) 
    if ret!=0:
      #FIXME: the failure happens on the fetch upstream, which we wont be catching given above chaining of commands
      acm_print("WARNING: Failed to synchronize local repo with upstream ... please check you had authentication permissions!")
    
  isPkg=False
  isExternal=False
  # by default, the packages in this repo/sm will be filtered out of the compilation
  with open(os.environ['ACM_PACKAGE_FILTER_FILE'], "a") as myfile:
    os.utime(os.environ['ACM_PACKAGE_FILTER_FILE'],None) #touches the file

    #only veto the subproject dir itself, if it contains a CMakeLists.txt
    if os.path.isfile(os.environ['SourceArea']+"/" + fullProjName + "/CMakeLists.txt"):
      #don't filter if there is a src or Root dir though .. as we assume that means it's a package
      if not (os.path.isdir(os.environ['SourceArea']+"/" + fullProjName + "/src") or os.path.isdir(os.environ['SourceArea']+"/" + fullProjName + "/Root")):
        myfile.write("- %s\n" % fullProjName)
        #check if there is a Find*.cmake file in the dir ...
        if len(glob.glob(os.environ['SourceArea']+"/"+fullProjName+"/Find*.cmake"))>0:
          isExternal=True
      else:
        isPkg=True
    if not isPkg: myfile.write("- %s/.*\n" % fullProjName)
  
  if isExternal:
    #add line to the project CMakeLists.txt so that the find_package files can be found ..
    #add at top of file
    with open(os.environ['SourceArea']+"/CMakeLists.txt",'r+') as f:
      content = f.read()
      f.seek(0,0)
      f.write("list( APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/" + fullProjName + " )\n" + content)
    acm_print("Set up external project %s" % fullProjName)
    acm_print("Use the cmake find_package function to utilise this project")
    acm_print("If project contains packages you can also use 'acm add_pkg %s/SomePackage' (or edit the package_filters.txt) to add packages" % fullProjName)
  elif not isPkg:
    acm_print("Set up project %s" % fullProjName)
    acm_print("Initially, packages in this project are filtered out.")
    acm_print("Use 'acm add_pkg %s/SomePackage' (or edit the package_filters.txt) to add packages" % fullProjName)
  else:
    acm_print("Set up and added package project %s" % fullProjName)
  sys.exit(0)


def acm_sparse_clone_project(argv):
  if len(argv)==1 and argv[0]=="-h":
    print("sparse_clone_project <name> <repo> [<tag/branch>]:")
    print("  Use this command to perform a sparse checkout of a git repository")
    print("  <name> is the name of the directory you will checkout the repo into (under $SourceArea)")
    print("  <repo> is the path to the repository. It can be a local path, or a remote (http) path.")
    print("         Note that you can omit the usual gitlab.cern.ch part in the path, e.g.")
    print("           mypath/myrepo.git   will try to check out https://:gitlab.cern.ch/mypath/myrepo.git")
    print("         If you prefer to use ssh, you can do: export ACM_GITLAB_PREFIX_PATH=ssh://:@gitlab.cern.ch:7999/")
    print("  <tag/branch> is the branch you want. If you don't specify then it will start in the master branch")
    print("")
    print(" SPECIAL CASE: <name>=athena ")
    print("  In this case you do not need to specify the <repo> IF your gitlab ")
    print("  username is the same as your local username.")
    print("  If it is not, you can use: ")
    print("")
    print("     acm sparse_clone_project athena cernUsername/athena.git")
    print("")
    print("  To checkout your athena fork, replacing cernUsername with your username")
    print("")
    return 0

  clone_project(argv,doSparse=True)

def acm_clone_project(argv):
  if len(argv)==1 and argv[0]=="-h":
    print("clone_project <name> <repo> [<tag/branch>]:")
    print("  Use this command to perform a checkout of a git repository")
    print("  <name> is the name of the directory you will checkout the repo into (under $SourceArea)")
    print("  <repo> is the path to the repository. It can be a local path, or a remote (http) path.")
    print("         Note that you can omit the usual gitlab.cern.ch part in the path, e.g.")
    print("           mypath/myrepo.git   will try to check out https://:gitlab.cern.ch/mypath/myrepo.git")
    print("         If you prefer to use ssh, you can do: export ACM_GITLAB_PREFIX_PATH=ssh://:@gitlab.cern.ch:7999/")
    print("  <tag/branch> is the branch you want. If you don't specify then it will start in the master branch")
    print("")
    print(" SPECIAL CASE: <name>=athena ")
    print("  In this case you do not need to specify the <repo> IF your gitlab ")
    print("  username is the same as your local username.")
    print("  If it is not, you can use: ")
    print("")
    print("     acm clone_project athena cernUsername/athena.git")
    print("")
    print("  To checkout your athena fork, replacing cernUsername with your username")
    print("")
    return 0

  clone_project(argv,doSparse=False)


# Two types of packages can be `rm`d:
#   1- standalone package (those that aren't a part of a project with many packages)
#   2- project package (e.g. an Athena package)
# Both need to be handled slightly differently.
def acm_add_pkg(argv) :
  if len(argv)==1 and argv[0]=="-h":
    print("""add_pkg [-f] <pkg>:
  Include <pkg> in compilation.
  If <pkg> is part of a repo, the filter file is updated to include "+ <pkg>/*.". Otherwise,
  if <pkg> was previously marked for exclusion, it is removed from the filters file.
  Also will ensure that if the package you are adding lives in a sparse checkout repo
  that the package source code is checked out properly.
  
  Use the -f option to force the inclusion of a package even if its not checked out properly""")
    return 0

  from sets import Set
  from utils import acm_print
  import os
  import subprocess

  filterFile = os.environ['ACM_PACKAGE_FILTER_FILE']

  # get the original data from filter file
  with open(filterFile, 'r') as original: data = original.readlines()

  forceAdd = False
  if len(argv)>0 and argv[0]=="-f":
    argv = argv[1:]
    forceAdd = True

  for i in reversed(range(0, len(argv))) :
    if argv[i].endswith("/"): argv[i] = argv[i].rstrip("/")
    rm_line = ("- %s\n" % argv[i])
    add_line = ("+ %s\n" % argv[i])
    if rm_line in data :
      data.remove(rm_line)
    if add_line not in data :
      #handle sparse-checkout case ...
      #check if sparse-checkout is enabled in the project ... if it exists then add the file       
      #only do if no wildcarding for now       
      if "*" not in argv[i]:         

        #get the project name (anything up to first /)         
        projName = argv[i].split("/")[0]         
        if os.path.isfile(os.environ['SourceArea']+"/"+projName+"/.git/info/sparse-checkout"):           
          #detected sparse checkout ... add the package           
          with open(os.environ['SourceArea']+"/"+projName+"/.git/info/sparse-checkout","a") as file:             
            file.write("\n"+argv[i].split("/",1)[1])           
          #refresh the tree
          subprocess.call("cd $SourceArea/%s;git read-tree -m -u HEAD" % projName,shell=True) 
          if not os.path.exists(os.environ['SourceArea']+"/"+argv[i]):
            acm_print("Checkout of %s failed :-(" % argv[i])
          else:
            acm_print("Checked out %s" % argv[i])
        
        #check the package exists (wont if the user made a typo)
        if not os.path.exists(os.environ['SourceArea']+"/"+argv[i]):
          acm_print("WARNING : %s does not exist  ... perhaps you have a typo " % argv[i])
          acm_print("          Remember that if your package is part of a project you checked out")
          acm_print("          you must include the project name as part of the path, e.g.")
          acm_print("            acm add_pkg athena/Control/AthAnalysisBaseComps")
          acm_print("")
          if not forceAdd:
            acm_print("Unable to add pkg %s ... use the -f option to force" % argv[i])
            continue
        elif not os.path.isfile(os.environ['SourceArea']+"/"+argv[i]+"/CMakeLists.txt"):
          #if a CMakeLists.txt file exists we might have checked out a directory of packages
          acm_print ("Warning: `%s` is not a package." % argv[i])
          acm_print ("         If it is a directory of packages you can include all with: add_pkg %s/.\\*" % argv[i])
          continue

      # mark pkg for inclusion
      data.insert(0,add_line)
      acm_print ("Package `%s` marked for inclusion in compilation." % argv[i])
    else :
      acm_print("Package `%s` already included in compilation." % argv[i])
 
  modified = open(filterFile, 'w')
  for p in data :
    modified.write("%s" % p)

def acm_rm_pkg(argv) :
  if len(argv)==1 and argv[0]=="-h":
    print("""rm_pkg <pkg>:
  Adds <pkg> to the package_filters.txt file with a preceding "-", marking it for exclusion from compilation.""")
    return 0

  from sets import Set
  from utils import acm_print
  import os

  filterFile = os.environ['ACM_PACKAGE_FILTER_FILE']

  # get the original data from filter file
  with open(filterFile, 'r') as original: data = original.readlines()

  for i in reversed(range(0, len(argv))) :
    rm_line = ("+ %s\n" % argv[i])
    add_line = ("- %s\n" % argv[i])
    if rm_line in data :
      data.remove(rm_line)
    if add_line not in data :
      # mark pkg for exclusion
      data.insert(0,add_line)
      acm_print ("Package `%s` marked for exclusion from compilation." % argv[i])
    else :
      acm_print("Package `%s` already excluded from compilation." % argv[i])
 
  modified = open(filterFile, 'w')
  for p in data :
    modified.write("%s" % p)
  

## Cleans all packages current acm workspace.
## TODO: implement it
def acm_clean(argv) :
  if len(argv)==1 and argv[0]=="-h":
    print("""clean [-f]: Cleans all targets. 
    If -f option (full clean) is specified, will also clean the TestArea, 
    and reconfigure""")
    return 0

  import subprocess
  from utils import acm_print

  if len(argv)==1 and argv[0]=="-f":
    acm_find_packages([])

  
  acm_print("Executing: cmake --build $TestArea --target clean ... ")
  ret = subprocess.call("cmake --build $TestArea --target clean",shell=True)
  sys.exit(ret)
  


## Compiles (i.e. builds) all packages in the current acm workspace.
## TODO: implement
def acm_compile(argv) :
  if argv != [] :
    print "acm_compile takes no arguments; compiling entire workspace"

  #for now, we just do
  #Call: cmake --build $TestArea
  import subprocess
  from utils import acm_print
  acm_print("Executing: cmake --build $TestArea ... ")
  ret = subprocess.call("cmake --build $TestArea",shell=True)

  sys.exit(ret)

## Compiles list, argv, of package(s).
## TODO: implement
def acm_compile_pkg(argv) :
  # Check for at least one list element
  if len(argv) > 0 :
    import subprocess
    from utils import acm_print
    acm_print("Executing: cmake --build $TestArea --target Package_%s... " % argv[0])
    ret = subprocess.call("cmake --build $TestArea --target Package_%s" % argv[0],shell=True)
    sys.exit(ret)
  else :
    print "compile_pkg requires at least one argument: package name"
    sys.exit(1)

## Create new package, with an alg and a joboption
## For now rely on acmd implementation, bt should extract it in future
def acm_new_skeleton(argv):
  import os
  from utils import acm_print
  if "AnalysisBase_DIR" in os.environ:
    acm_print("Sorry, the new_skeleton command is not available in AnalysisBase. All other acm commands are available, however. Or you could try switching to AthAnalysis")
    sys.exit(0)

  if len(argv) > 0 :
    packageName = argv[0] #will include path  
    import subprocess
    acm_print("Executing: cd $SourceArea; acmd cmake new-skeleton %s ... " % argv[0])
    ret = subprocess.call("cd $SourceArea; acmd cmake new-skeleton %s" % argv[0],shell=True)
    if ret!=0: sys.exit(ret)
    acm_find_packages([]) #reconfigure to avoid problem with centos7


## Creates a new empty package.
## Makes a clean directory in the acm `sourcedir` and with template
## CMakeLists.txt.
## TODO: implement
def acm_new_pkg(argv) :
  if len(argv) > 0 :

    packageName = argv[0] #will include path

    from utils import acm_print

    import os

    sourcedir = os.environ['SourceArea']

    #1. Check for existence of package in testarea already
    if os.path.isdir(sourcedir+"/"+packageName):
      # print ("Package '%s' already exists. Cannot create new package" % packageName)
      sys.exit(1)

    #2. Create directory structure
    try:
      os.makedirs(sourcedir+"/"+packageName+"/src")
      os.makedirs(sourcedir+"/"+packageName+"/share")
      os.makedirs(sourcedir+"/"+packageName+"/python")
      os.makedirs(sourcedir+"/"+packageName+"/data")
      os.makedirs(sourcedir+"/"+packageName+"/util")
    except OSError:
      print ("Error when creating directories for package at %s/%s" % (sourcedir,packageName))
      sys.exit(1)


    #3. Create skeleton CMakeLists.txt file
    pkg_name = os.path.basename(packageName)

    templateFile = open( os.environ["ACMDIR"] + "/python/templates/new_CMakeLists.txt" )
    myTemplate = templateFile.read()

    with open(os.path.join(sourcedir+"/"+packageName,'CMakeLists.txt'),"w") as req:
      print >> req, myTemplate%locals()



    acm_print ("Created package '%s' ... " % packageName)
    acm_print ("Calling: acm find_packages")
    ret = acm_find_packages([])
    if ret:
        sys.exit(ret)

    sys.exit(0)
  else :
    print ("new_pkg requires at least one argument: package name")
    sys.exit(1)


def acm_find_packages(argv) :
  import os

  from utils import acm_print
  import subprocess

  myFilter="$ACM_PACKAGE_FILTER_FILE"
  if len(argv) > 0 :
    if argv[0]=="-h":
      print("""find_package [<package_filter_file>] : reconfigure your cmake build, using the optionally provided
package filter file. If no filter file is provided, will use the file pointed to by $ACM_PACKAGE_FILTER_FILE""")
      return 0
    myFilter = argv[0]
      


  #had to make this a hard cleanup of cmake, because it seemed that when compiling an externals project, the project wasn't picked up unless I deleted the cmake files too
  mycmd = "cd $TestArea;rm -r CMakeFiles;rm CMakeCache.txt;cmake -DATLAS_PACKAGE_FILTER_FILE=%s $SourceArea" % myFilter

  acm_print ("Calling: %s" % mycmd)
 
  ret = subprocess.call(mycmd,shell=True)
  if ret:
        print ("... find_packages failed :-( ... see output above!")
        sys.exit(1)
  
  sys.exit(0)


#code for this function borrowed from  https://gitlab.cern.ch/atlas/atlasexternals/blob/master/Build/AtlasCMake/scripts/acmake.py
#this function will determine all clients and call add_pkg for each of them
def acm_add_pkg_clients(argv) :
    import os
    import re

    from utils import acm_print
    import subprocess

    if len(argv) > 0 :
      if argv[0]=="-h":
        print("""add_pkg_clients <pkg> : add_pkg for all clients of the specified pkg""")
    else:
      print("""ERROR: You must specify a package""")
      return -1

    fullPackage = argv[0]

    #take only the last part as the package name
    #first part will be the project
    projectName = fullPackage.split("/")[0]
    package = fullPackage.split("/")[-1]

    #check project exists ...
    if not (os.path.isdir(os.environ["SourceArea"]+"/"+projectName) or os.path.exists(os.environ["SourceArea"]+"/"+projectName)):
      acm_print("ERROR: Could not find subproject %s -- please remember to specify the full path for the package (e.g. athena/Control/AthAnalysisBaseComps)" % projectName)
      return -1


    #obtain a list of packages residing in the same subproject
    myproc = subprocess.Popen("cd $SourceArea/%s;git ls-tree --name-only -r --full-tree HEAD | grep CMakeLists.txt" % projectName,stdout=subprocess.PIPE,shell=True,close_fds=True)
    allPackages = myproc.stdout.readlines()
    #strip the CMakeLists.txt\n off the end of each entry
    allPackages = [pkg.replace('/CMakeLists.txt\n','') for pkg in allPackages]
    

    
    


    # Get the CMAKE_PREFIX_PATH environment variable. As we'll be searching
    # in that.
    ppath = os.getenv( "CMAKE_PREFIX_PATH" )
    if not ppath:
        print( "ERROR: No release seems to be set up" )
        return 1

    # Keep track of which directories were already visited:
    releaseDirectories = []

    # Target file finding regular expression:
    target_re = re.compile( ".*-targets.cmake$" )

    # Collect the names of all the files that the code will need to look at:
    target_files = []
    for rdir in ppath.split( ":" ):
        # Skip it if we already looked at this directory:
        if rdir in releaseDirectories:
            continue
        # Look for a file called *-targets.cmake in its cmake/ subdirectory:
        cmake_dir = os.path.join( rdir, "cmake" )
        if not (os.path.isdir(cmake_dir) or os.path.exists(cmake_dir)):
            continue
        tfiles = [ f for f in os.listdir( cmake_dir )
                   if os.path.isfile( os.path.join( cmake_dir, f ) ) and
                   target_re.search( f ) ]
        if len( tfiles ) != 1:
            print( "WARNING: Multiple target files found in directory %s" % \
                       cmake_dir )
            print( "WARNING: Found files: %s" % str( target_files ) )
            pass
        # Add this to the overall list:
        target_files += [ os.path.join( cmake_dir, f ) for f in tfiles ]
        pass


    ## Sub-function collecting the clients of one given package
    #
    # @param package The name of the package to collect the clients of
    # @param public Set to <code>True</code> to look for public dependencies,
    #               or to <code>False</code> to look for private ones
    # @param recursive Set to <code>True</code> to look for (public) clients
    #                  recursively
    # @returns A set with the names of the requested clients
    #
    def collect_clients( package, public, recursive ):
        # A small sanity check:
        if not public and recursive:
            print( "ERROR: It doesn't make sense to ask for private clients "
                   "recursicely!" )
            return set([])
        # Construct the regular expression to use:
        client_re = ""
        if public:
            client_re = \
                "^set_target_properties\(\w+::(\w+)Pkg PROPERTIES[^\)]*" \
                "INTERFACE_LINK_LIBRARIES \"(?:[^\"]*[:;]+)?%sPkg[^\"]*\"" % \
                package
        else:
            client_re = \
                "^set_target_properties\(\w+::(\w+)PkgPrivate PROPERTIES[^\)]*" \
                "INTERFACE_LINK_LIBRARIES \"(?:[^\"]*[:;]+)?%sPkg[^\"]*\"" % \
                package
            pass
        # The result list:
        result = set([])
        # Loop over the files:
        for fname in target_files:
            # Open the file:
            target_file = open( fname, "r" )
            # Take the direct clients of this package:
            clients = re.findall( client_re, target_file.read(),
                                  re.MULTILINE )
            # If recursion is asked for, do that:
            if recursive:
                for client in clients:
                    for pkg in collect_clients( client, public, True ):
                        result.add( pkg )
                        pass
                    pass
                pass
            # Finally, add the direct dependencies:
            for client in clients:
                result.add( client )
                pass
            pass
        # Return the resulting set:
        return result

    # Collect results
    from sets import Set
    all_clients = Set()
    all_clients.update(collect_clients( package, True, True ))
    all_clients.update(collect_clients( package, False, False )) #direct private clients
    all_clients.update(collect_clients( package, True, False )) #direct public clients
    
    for client in all_clients:
      clientFullName = projectName + "/"
      found=False
      for pkg in allPackages:
        if pkg.endswith(client): 
          clientFullName += pkg
          found=True
          break

      if not found:
        acm_print("WARNING: Could not find client package %s" % client)
      else:
        acm_add_pkg([clientFullName])

    # Return gracefully:
    return 0

