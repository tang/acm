from __future__ import print_function
#!/usr/bin/env python

# @file     utils.py
# @purpose  Provides functionality related to configuring and modifying an acm workarea.
# @author   Vincent Pascuzzi <vincent.pascuzzi@cern.ch, vpascuzz@physics.utoronto.ca>
# @date     May 2017

import os
import sys
import string
from acmcommands import get_std_cmds_dict

## Create a string for acm environment exports.
def get_exports(sourcePath, buildPath, acmbin, acmpython, acmscripts, gitlabprefix) :
  expstr = ""
  # Format the export string
  expstr += "export ACMSOURCEDIR=%s\n"            % sourcePath
  expstr += "export ACMBUILDDIR=%s\n"             % buildPath
  expstr += "export SourceArea=%s\n"              % sourcePath
  expstr += "export TestArea=%s\n"                % buildPath
  expstr += "export ACMBIN=%s\n"                  % acmbin
  expstr += "export PATH=%s:$PATH\n"              % acmbin
  expstr += "export PYTHONPATH=%s:$PYTHONPATH\n"  % acmpython
  expstr += "export PYTHONPATH=%s:$PYTHONPATH\n"  % acmscripts
  expstr += "export ACM_GITLAB_PATH_PREFIX=%s\n"  % gitlabprefix
  # Return export string
  return expstr

## Creates a string of the environment variables to unset.
def get_unsets(acmbin, acmpython, acmscripts) :
  unstr = ""
  # Format the unset string
  unstr += "unset ACMBIN\n"
  unstr += "unset ACMSOURCEDIR\n"
  unstr += "unset ACMBUILDDIR\n"
  unstr += remove_from_path("PATH",       [acmbin])
  unstr += remove_from_path("PYTHONPATH", [acmpython,acmscripts])
  # Return unset string
  return unstr

## Remove `path` from `ex`
def remove_from_path(path, ex) :
  restoredpath = ""
  old = os.getenv(path)
  if old != None :
    mypath = string.split(old,":")
    mypath[:] = [x for x in mypath if x not in ex]
    restoredpath = "export %s=%s\n" % (path, string.join(mypath, ":"))
    pass
  # Format the export string
  # Return the path without `ex`
  return restoredpath

## Respectfully stolen from rc internals
def ask_question (question, answers) :
  """effects: ask the given question repeatedly until I receive one
  of the given answers
  returns: the answer given"""
  answer = None
  while not answer in answers:
    sys.stdout.write (question + " (" + "/"  .join (answers) + "): ")
    answer = string.strip (sys.stdin.readline ())
    pass
  return answer

## Write `setupText` to setup file `filename`
def write_setup_file(setupText, configFilename=None, asetupOptions="",sourcePath="", reconfigure=False, doLocal="", doDocker="") :

  if len(doDocker):
    
    dockerRunFile = open(configFilename,'w')
    dockerRunFile.write("""if [[ "$(docker images -q %s 2> /dev/null)" == "" ]]; then\n""" % (doDocker))
    dockerRunFile.write("""  echo "ACM\tTrying to pull docker image %s (docker image pull %s)... "\n""" % (doDocker,doDocker))
    dockerRunFile.write("""  docker image pull %s\n""" % doDocker)
    dockerRunFile.write("""  if [ $? -ne 0 ]; then\n     return 1\n  fi\n\n""")
    dockerRunFile.write("""fi\n""")
    #dockerRunFile.write("""xhost + $(hostname)\n""")
    #dockerRunFile.write("""docker run --rm -v $HOME:$HOME -e HOME=$HOME -e ACMDIR=$ACMDIR -e DISPLAY=$(hostname):0 -w $PWD -it %s /bin/bash --rcfile .dockerSetup.sh\n""" % doDocker)
    dockerRunFile.write("""xhost + 127.0.0.1\n""")                                                                   
    #FIXME: we really need to support non-mac users here!
    dockerRunFile.write("""docker run --rm --net=host -v $HOME:$HOME -e HOME=$HOME -e ACMDIR=$ACMDIR -e DISPLAY=docker.for.mac.host.internal:0 -w $PWD -it %s /bin/bash --rcfile .dockerSetup.sh\n""" % doDocker)    
    dockerRunFile.close()

    #write everything to a local file ..
    configFile = open(".dockerSetup.sh",'w')
    configFile.write("export ReleaseSetupScript=/home/atlas/release_setup.sh\n")

  else:
    configFile = open(configFilename, 'w')

  doGit=True #FIXME: make this an option of the method

  #need to extract the branch

  ###
  ## setup the release
  ###

  # for unclear reasons, ACMDIR sometimes get clobbered by asetup
  # so save it here
  configFile.write("""ACMDIRBACKUP=$ACMDIR\n""");

  # check if arguments were passed for `asetup`; if not, we assume a local setup
  if len(doLocal)==0:
    # TODO: We are on an asetup-aware node -- remove following line
    asetupArg = "'asetup %s'" % asetupOptions
    configFile.write("""echo "ACM\tExecuting %s ..."\n""" % asetupArg)
    # check if `asetup` is ready for use, i.e. `setupATLAS` has been called
    configFile.write("""if ! command -v asetup > /dev/null 2>&1; then echo "ACM\t\`asetup\` not found (did you run \`setupATLAS\`?) -- exiting."; unset ACMDIR; return 1; fi\n""")
    # export after `asetup` is found, otherwise environment will think acm is setup (but it failed without asetup)
    #configFile.write("%s" % setupText)

    #print setupText
    #configFile.write("echo $ACMBUILDDIR\n")

    configFile.write("echo '************************************************************************'\n")
    configFile.write("asetup %s\n" % asetupOptions)
    configFile.write("asetupResult=$?\n")
    configFile.write("echo '************************************************************************'\n")
    # also setup git, needed for sparse_clone_project:
    configFile.write("lsetup git\n")
    pass
  else:
    #doing local release setup, so just try to source the setup script ...
    configFile.write("""echo "ACM\tLocal release setup..."\n""")
    configFile.write(doLocal)
    import os
    if "ReleaseSetupScript" in os.environ or doDocker:
      configFile.write("""echo "Executing: source $ReleaseSetupScript ..."\n""")
      configFile.write("echo '************************************************************************'\n")
      configFile.write("source $ReleaseSetupScript\n")
    else:
      configFile.write("""echo "Executing: source $ReleaseArea/$AtlasProject/$AtlasVersion/InstallArea/*/setup.sh ..."\n""")
      configFile.write("echo '************************************************************************'\n")
      configFile.write("source $ReleaseArea/$AtlasProject/$AtlasVersion/InstallArea/*/setup.sh\n")
    configFile.write("asetupResult=$?\n")
    configFile.write("echo '************************************************************************'\n")

  #check setup was ok
  configFile.write("if [ $asetupResult -ne 0 ]; then\n")
  configFile.write(""" echo "ACM\tRelease setup failed. Please check your arguments: %s"\n""" % asetupOptions)
  configFile.write(" return $asetupResult\n")
  configFile.write("fi\n\n")

  configFile.write("echo '************************************************************************'\n")

  configFile.write("""export ACMDIR=$ACMDIRBACKUP\n""")

  if setupText is None and sourcePath is None :
    configFile.write("""echo "ACM\tWARNING: A valid SourceArea was not found, so acm commands are not available, only the release was setup."\n""")
    return

  # export after `asetup` is found, otherwise environment will think acm is setup (but it failed without asetup)        # for easons I do not understand, asetup seems to be able to remember the values of even the $ACMBUILDDIR env var?
  configFile.write("%s" % setupText)

  #configFile.write("echo $ACMBUILDDIR\n")

  #ensure TestArea put back to ACMBUIDDIR
  configFile.write("export TestArea=$ACMBUILDDIR\n")
  


  #Make the source dir
  #needs to depend on git ... FIXME ... need better way to detect the 'release' than relying on AtlasBuildBranch
  configFile.write("if [ ! -f $ACMSOURCEDIR/CMakeLists.txt ]; then\n")
  configFile.write(" echo 'ACM\tNo project file in source dir ... Adding a project CMakeLists.txt ..'\n")
  configFile.write(" cp $ACMDIR/python/templates/new_project_CMakeLists.txt $ACMSOURCEDIR/CMakeLists.txt\n")
  configFile.write("fi\n")

  # check if package filter file exists
  # prioritise filter files in the $SourceArea over the $TestArea
  configFile.write("if [ ! -f $ACMSOURCEDIR/package_filters.txt ]; then\n")
  configFile.write(" export ACM_PACKAGE_FILTER_FILE=$ACMBUILDDIR/package_filters.txt\n")
  configFile.write(" if [ ! -f $ACMBUILDDIR/package_filters.txt ]; then\n") #AAA
  configFile.write("  touch $ACMBUILDDIR/package_filters.txt\n")

  # need to start off by ignoring all subprojects, which are directories under $SourceArea that contain .git
  import os
  subdirs = [o for o in os.listdir(sourcePath) if os.path.isdir(os.path.join(sourcePath,o,".git")) ]
  for s in subdirs: configFile.write("  echo '- %s/.*' >> $ACMBUILDDIR/package_filters.txt\n" % s)

  configFile.write(" fi\n"); #end AAA
  configFile.write("else\n"); #has a package_filters.txt in the SourceArea
  configFile.write(" export ACM_PACKAGE_FILTER_FILE=$ACMSOURCEDIR/package_filters.txt\n");
  configFile.write("fi\n");

  # Auto install of build dir
  if reconfigure:
    configFile.write('echo "ACM\tChange of release detected, rerunning cmake configuration ..."\n')
    configFile.write('echo "ACM\tRunning: rm -r CMakeFiles;rm CMakeCache.txt;cmake -DATLAS_PACKAGE_FILTER_FILE=$ACM_PACKAGE_FILTER_FILE $ACMSOURCEDIR..."\n')
    configFile.write('cd $ACMBUILDDIR;rm -r CMakeFiles;rm CMakeCache.txt;cmake -DATLAS_PACKAGE_FILTER_FILE=$ACM_PACKAGE_FILTER_FILE $ACMSOURCEDIR;cd -\n') #FIXME: check config succeeds!  
  else:
    configFile.write('if ls $ACMBUILDDIR/*/setup.sh 1> /dev/null 2>&1; then\n')
    configFile.write(' echo "ACM\t$ACMBUILDDIR is already configured"\n')
    configFile.write('else\n')
    configFile.write(' echo "ACM\tConfiguring $ACMBUILDDIR";cd $ACMBUILDDIR\n')
    configFile.write(' echo "ACM\tRunning: cmake -DATLAS_PACKAGE_FILTER_FILE=$ACM_PACKAGE_FILTER_FILE $ACMSOURCEDIR..."\n')
    configFile.write(' cmake -DATLAS_PACKAGE_FILTER_FILE=$ACM_PACKAGE_FILTER_FILE $ACMSOURCEDIR; cd -\n') #FIXME: check config succeeds!  
    configFile.write('fi\n')

  configFile.write('if ls $ACMBUILDDIR/*/setup.sh 1> /dev/null 2>&1; then\n')
  configFile.write(' echo "ACM\tRunning: source $ACMBUILDDIR/*/setup.sh"\n')
  configFile.write(' source $ACMBUILDDIR/*/setup.sh\n')
  configFile.write('else\n')
  configFile.write(' echo "ACM\tERROR There was a problem with your cmake configuration. acmSetup unable to finish. See cmake messages above"; return 1;\n ')
  configFile.write('fi\n')
  

  # remove load balancing (to speed up compilation)
  configFile.write("export MAKEFLAGS=`echo $MAKEFLAGS | sed 's/-l.*//g'`\n")

  # Auto-completion ... only in bash
  std_cmd_keys_list = [cmdkey for cmdkey in get_std_cmds_dict()]
  configFile.write('if [ -n "$BASH_VERSION" ]; then\n')
  configFile.write(' alias acm="python `which acm`"\n') #added this line to support MAC users (see chat with TJ)
  configFile.write(' source $ACMBIN/../scripts/acm.bash-completion %s\n' % ' '.join(std_cmd_keys_list))
  configFile.write('fi\n')


  configFile.write('echo "ACM\tSetup complete. Use \`acm help\` to get started."\n')

  
  configFile.close()
  pass

## Write the file to source when unsetting an ACM environment.
## Also removes the `complete` entry for acm.
def write_unsetup_file(setupText, configFilename=None) :
  configFile = open(configFilename, 'w')
  std_cmd_keys_list = [cmdkey[0] for cmdkey in get_std_cmds_dict()]
  # Remove `complete` entry
  configFile.write('complete -r acm\n')
  configFile.write("%s" % setupText)
  configFile.close()
  pass

## function that searches for an existing ACM setup configuration file,
## .acmsetup.save. The cwd and all parents are searched. If the root
## directory, '/', is reached, we have not found an existing setup.
def acmsetup_search() :
  configFile = ".acmsetup.save"
  curDir = os.getcwd() # where we start searching
  while True :
    parentDir = os.path.dirname(curDir)
    if configFile in os.listdir(curDir) :
      return curDir
    else:
     # Have we reached the root directory?
      if curDir == parentDir :
        return None
      # Otherwise, update curDir and keep searching
      else :
        curDir = parentDir

## Makes a directory `dirname`
def mkdir(dirname) :
  try:
    os.mkdir(dirname)
  except OSError:
    print ("ERROR cannot make %s directory!" % dirname)
    sys.exit(1)

def acm_print(str, continueline=False) :
  if continueline :
    print ("ACM\t%s" % str, end=''),
  else:
    print ("ACM\t%s" % str)
  pass
